module.exports = {

    /**
     * logMissingIdenErrToConsole - logs that indentifier is missing to console
     * @param name - name of current entity
     */
    logMissingIdenErrToConsole: function (name){
        console.log(`SyntaxError: Identifier attribute is missing.\n\t`+
            `at entity: ${name}\n\t`);
    },

    /**
     * logMissingIdenErrToConsole - logs that indentifier is incorrect to console
     * @param name       - name of current entity
     * @param identifier - identifier of current entity
     * @param fnameTmp   - file name
     */
    logIdenErrToConsole: function (name, identifier, fnameTmp){
        console.log(`SyntaxError: Identifier doesn't match file name.\n\t` +
            `at entity: ${name}\n\t` +
            `is       : ${identifier}\n\t` +
            `should be: ${fnameTmp}\n`);
    },

    /**
     * logParsingErrToConsole - logs that data model contains mapping error to console
     * @param name - name of current entity
     * @param err  - describes the syntax error
     * @param rule - current mapping rule
     */
    logParsingErrToConsole: function (rule, err, name){
        console.log(`${err}\n\t`+
            `at entity: ${name}\n\t`+
            `at mapping rule: ${rule}\n`);
    },

    /**
     * logEntityNameErrToConsole - logs that data model contains mapping error at entity name to console
     * @param name           - name of current entity
     * @param currentElement - incorrect entity name
     */
    logEntityNameErrToConsole: function (currentElement, name){
        console.log(`SyntaxError: Entity name in mapping rules does not match any entities.\n\t`+
            `at entity: ${name}\n\t` +
            `entity name not found: ${currentElement}\n`);
    },

    /**
     * logMeaErrToConsole - logs that data model contains mapping error at MEA annotation to console
     * @param currentElement - incorrect entity name
     * @param name           - name of current entity
     */
    logMeaErrToConsole: function (currentElement, name){
        console.log(`SyntaxError: Measurement name (MEA_*) in mapping rules does not match any existing MEA annotations.\n\t`+
            `at entity: ${name}\n\t` +
            `measurement name not found: ${currentElement}\n`);
    },

    /**
     * logErrorAmtToConsole - Prints amount of errors found in file to console
     * @param fname - name of current JSON file
     * @param errorAmt - amount of errors found in the current JSON file
     */
    logErrorAmtToConsole: function(fname, errorAmt){
        if (errorAmt.value === 1)
            console.log(`\n${errorAmt.value} error detected in file "${fname}".\t`);
        else
            console.log(`\n${errorAmt.value} errors detected in file "${fname}".\t`);
    }

}