const validators = require("./validators");
const errorLogger = require("./errorMsgs");
const fs = require("fs");


/**
 * compileDataEntities - calls all parsing and validating fuctions
 * @param taggingSet     - all information regarding one entity
 * @param fname          - file name
 * @param errorAmt       - variable for counting amount of errors
 * @param allEntityNames - names of all entities in the current data model
 */
function compileDataEntities(taggingSet, fname, errorAmt, allEntityNames) {
    const rulesRaw = taggingSet.rules;

    if (rulesRaw != null) {
        // splits string of rules into array of individual rules
        const arrRulesRaw = rulesRaw.split(/\r?\n|\r/);

        // parses and validates array of individual rules
        const parsedRules = arrRulesRaw.map(pr => validators.ruleValidator(pr, taggingSet.name, errorAmt))
            .filter(rule => rule !== undefined);

        // parses and validates entity names and MEA annotations of individual rules
        parsedRules.forEach( pr => validators.entityNameAndMeaParser(pr, allEntityNames, taggingSet.name, errorAmt));

        // parses and validates identifier of individual Data entities
        errorAmt.value += validators.identifierValidator(taggingSet,fname);
    }
}


    /**
     * prepareFile - reads JSON file and prepares it for parsing
     * @param dir       - directory of JSON file
     * @param  fname    - file name
     * @param  errorAmt - variable for counting amount of errors
     * */
function prepareFile(dir, fname, errorAmt) {
    try {
        const rawData = fs.readFileSync(dir + fname);
        const taggingSets = JSON.parse(rawData);

        //collects all entity names in the current data model
        const allEntityNames = taggingSets.classes.map(ts => validators.getEntityName(ts))
            .filter(name => name !== undefined);

        taggingSets.classes.forEach(ts => this.compileDataEntities(ts, fname, errorAmt, allEntityNames));

        errorLogger.logErrorAmtToConsole(fname, errorAmt);
    } catch(err) {
        console.error(err);
    }
}

module.exports = { prepareFile, compileDataEntities};