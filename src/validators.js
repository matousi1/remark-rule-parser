const parser = require("./generatedParser");
const namesMEA = require("./namesMEA");
const errorLogger = require("./errorMsgs");

module.exports = {

    /**
     * identifierValidator - validates identifiers, returns error when identifier is missing or doesn't match
     *                       name of the file
     * @param taggingSet - all information regarding one entity
     * @param fname      - file name
     * @returns {number} - amount of errors found
     */
    identifierValidator: function(taggingSet, fname) {
        const fnameTmp = fname.slice(0, -5); // cuts out ".json"
        const identifier = taggingSet.identifiers[0];
        if (identifier != null) {
            if (identifier !== fnameTmp) {
                // called when identifier doesn't match file name
                errorLogger.logIdenErrToConsole(taggingSet.name, identifier, fnameTmp);
                return 1;
            }
            return 0;
        } else {
            // called when no identifier found
            errorLogger.logMissingIdenErrToConsole(taggingSet.name, fnameTmp);
            return 1;
        }
    },

    /**
     * ruleValidator - parses and validates individual rules
     * @param rule     - individual mapping rule
     * @param name     - name of current entity
     * @param errorAmt - variable for counting amount of errors
     * @returns {*}    - parsed rule into tokens (undefined when error found)
     */
    ruleValidator: function  (rule, name, errorAmt) {
        try {
            if (rule !== "")
                return parser.parse(rule);
        } catch (err) {
            errorLogger.logParsingErrToConsole(rule, err, name);
            errorAmt.value += 1;
        }
    },

    /**
     * getEntityName - returns name of the entity, that doesn't end with "Data"
     * @param taggingSet - all information regarding one entity
     * @returns {*}      - returns name of the entity, when it doesn't end with "Data"
     */
    getEntityName: function (taggingSet){
        if (taggingSet.name.slice(-4) !== "Data")
            return taggingSet.name;
    },

    /**
     * meaValidator - checks if current MEA annotation is valid
     * @param currentElement - current MEA annotation
     * @param name           - current Data entity name
     * @returns {number}     - returns amount of errors found
     */
    meaValidator : function (currentElement, name){
        if (namesMEA.includes(currentElement)) {
            return 0;
        } else {
            errorLogger.logMeaErrToConsole(currentElement, name);
            return 1;
        }
    },

    /**
     * entityNameValidator - checks if current entity name is valid
     * @param currentElement - current entity name
     * @param entityNames    - all entity names
     * @param name           - current Data entity name
     * @returns {number}     - returns amount of errors found
     */
    entityNameValidator : function (currentElement,entityNames, name){
        if (entityNames.includes(currentElement)) {
            return 0;
        } else {
            errorLogger.logEntityNameErrToConsole(currentElement, name);
            return 1;
        }
    },

    /**
     * validateEntityNameAndMea - checks if passed string is a MEA annotation or an entity name
     * @param currentElement - current entity name or MEA annotation
     * @param entityNames    - all entity names
     * @param name           - current Data entity name
     * @returns {number}     - returns amount of errors found
     */
    validateEntityNameAndMea : function (currentElement,entityNames, name){
        if (currentElement.slice(0,4) !== "MEA_")
            return this.entityNameValidator(currentElement,entityNames, name);
        return this.meaValidator(currentElement, name);
    },

    /**
     * parseEntityNameAndMea - splits individual rules into individual entity names and MEA annotations
     * @param currentEntities - Object containing all MEA annotations and entity names of one mapping rule
     * @param allEntityNames  - all entity names
     * @param name            - current Data entity name
     * @returns {number}      - returns amount of errors found
     */
    parseEntityNameAndMea : function (currentEntities, allEntityNames, name){
        let localErrorAmt = 0;
        try {
            for (let i = 0; i < currentEntities.andOr.length; i++) {
                if (typeof currentEntities.andOr[i] !== "string") {
                    // when Array is found
                    // Recursively call the function and add its counter value to the current counter
                    localErrorAmt += this.parseEntityNameAndMea(currentEntities.andOr[i], allEntityNames, name, localErrorAmt);
                } else {
                    //when String is found
                    localErrorAmt += this.validateEntityNameAndMea(currentEntities.andOr[i], allEntityNames, name, localErrorAmt);
                }
            }
        } catch {
            // for when current object doesn't contain ".andOr" structure
            localErrorAmt += this.validateEntityNameAndMea(currentEntities, allEntityNames, name);
        }
        return localErrorAmt;
    },

    /**
     * entityNameAndMeaParser - takes entity names and MEA annotations out of parsedRule object
     * @param parsedRule     - individual rule that has been tokenized
     * @param allEntityNames - names of all entities
     * @param name           - current Data entity name
     * @param errorAmt       - variable for counting amount of errors
     * @returns {number}     - when current entity is not Data entity, ends with 0
     */
    entityNameAndMeaParser : function (parsedRule, allEntityNames, name, errorAmt){ // takes EntityNames and MEA_* out of parsedRule object
        if (name.slice(-4) !== "Data")
            return 0;

        let currentEntities;
        try{
            currentEntities = parsedRule.impliedRule.entities;
        }catch{
            currentEntities = parsedRule.entities;
        }

        errorAmt.value += this.parseEntityNameAndMea(currentEntities, allEntityNames, name);
    }

}