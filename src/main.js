const filePreparator = require('./filePreparation');

 try {
   // drectory of data model we want to check
   const dir = "./modelsJSON/";
   let fname = process.argv[2];
   var errorAmt = {value: 0};

   filePreparator.prepareFile(dir, fname, errorAmt);

 } catch (err) {
   console.log(err);
 }