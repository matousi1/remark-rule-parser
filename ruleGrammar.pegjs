start
  = rule
  
rule
  = lbr condition:condition rbr arrow lbr impliedRule:singleRule rbr terminator { return { condition, impliedRule }; }
  / singleRule:singleRule terminator { return singleRule; }
  
  
arrow = [ ]* "->" [ ]*
lbr = [ ]* "(" [ ]*
rbr = [ ]* ")" [ ]*
terminator = [ ]* ";" [ ]*
  
singleRule
  = condition:condition arrow entities:entities { 
    return { condition, entities }; 
  }

condition
  = columnCond:columnCond restOfExpr:((andConn / orConn) columnCond)* 
  { const mkExpr = (cond, rest) => {
     if (rest.length === 0) {
       return cond;
     } else {
       const [connective, nextExpr] = rest[0];
       return { [connective]: [ cond, mkExpr(nextExpr, rest.slice(1)) ] };
     }
   }
    
    return mkExpr(columnCond, restOfExpr);
  }
  
andConn = [ ]* "AND" [ ]* { return "andOr"; }
orConn = [ ]* "OR" [ ]* { return "andOr"; }
  
columnCond
  = colName:colName colValExp:((eq / notEq) colVal)?
  { return colValExp !== null ? { column: colName, value: colValExp[1] } : { column: colName }; }

eq = [ ]* "=" [ ]*
notEq = [ ]* "!=" [ ]*

colName
  = string / regex

colVal
  = string / regex / number
  
string "string"
  = '"' str:[^"]* '"' { return str.join(""); }
  
regex "regex"
  = '/' reg:[a-zA-Z0-9 _.+*]+ '/' { return "/" + reg.join("") + "/"; }
  
 number "number"
  = digits:[0-9]+ { return parseInt(digits.join(""), 10); }
  
entities
  = entity:entity restOfEntities:((andConn / orConn) entity)* 
  { const mkExpr = (entity, rest) => {
     if (rest.length === 0) {
       return entity;
     } else {
       const [connective, nextExpr] = rest[0];
       return { [connective]: [ entity, mkExpr(nextExpr, rest.slice(1)) ] };
     }
   }
    
    return mkExpr(entity, restOfEntities);
  }
 
 entity = string

