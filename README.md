# Instrukce pro spuštění programu

1. Ujistěte se, že soubor, který chcete zkontrolovat, je typu JSON.
2. Soubor musí být umístěn ve složce "modelsJSON".
3. Spusťte program v PowerShellu pomocí tohoto příkazu ve složce s exe souborem:

   ```
   .\otprocessor-win.exe název_souboru.json
   ```

   Nahraďte "název_souboru" názvem souboru, který chcete zkontrolovat.

4. Výsledek bude vypsán do terminálu.

## Chyby, které program označí


- Chybná syntaxe datových/mapovacích pravidel.
```
SyntaxError: Expected "->", "AND", "OR", or [ ] but "-" found.
  at entity: Ubytování v hotelu Data
  at mapping rule: "stapro_kod" != 2654 - "Počet turistů ubytovaných v hotelu";
```
- Chybějící identifikátor.
```
SyntaxError: Identifier attribute is missing.
        at entity: Ubytovací zařízení Data
```
- Identifikátor, který je rozdílný od názvu souboru.
```
SyntaxError: Identifier doesn't match file name.
        at entity: Referenční období Data
        is       : hoste-a-prenocovani-v-hotelich-podle-zem
        should be: hoste-a-prenocovani-v-hotelich-podle-zemi
```
- Překlepy v názvu entit v mapovacích pravidlech.
```
SyntaxError: Entity name in mapping rules does not match any entities.
        at entity: Referenční období Data
        entity name not found: Referenční obdob
```
- Překlepy v názvu MEA_* v mapovacích pravidlech.
```
SyntaxError: Measurement name (MEA_*) in mapping rules does not match any existing MEA annotations.
        at entity: Referenční období Data
        measurement name not found: MEA_Časový okamži
```