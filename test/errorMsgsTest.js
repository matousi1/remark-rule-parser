const assert = require('assert');
const sinon = require('sinon');
const errorLogger = require('../src/errorMsgs');

describe('errorMsgs', function() {
    const entityName = 'TestEntity';
    let sandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    describe('logMissingIdenErrToConsole', function () {
        it('should log an error message with the entity name', function () {
            //prepare test data
            const expected = `SyntaxError: Identifier attribute is missing.\n\tat entity: ${entityName}\n\t`;

            // prepare Stub functions
            const consoleLogSpy = sandbox.spy(console, 'log');

            //run tested function
            errorLogger.logMissingIdenErrToConsole(entityName);

            // check results
            assert(consoleLogSpy.calledWith(expected));
        });
    });

    describe('logIdenErrToConsole', function () {
        it('should log an error message with the entity name, identifier, and expected identifier', function () {
            //prepare test data
            const identifier = 'ABC';
            const expectedIdentifier = 'DEF';
            const expected = `SyntaxError: Identifier doesn't match file name.\n\tat entity: ${entityName}\n\tis       : ${identifier}\n\tshould be: ${expectedIdentifier}\n`;

            // prepare Stub functions
            const consoleLogSpy = sandbox.spy(console, 'log');

            //run tested function
            errorLogger.logIdenErrToConsole(entityName, identifier, expectedIdentifier);

            // check results
            assert(consoleLogSpy.calledWith(expected));
        });
    });

    describe('logParsingErrToConsole', function () {
        it('should log an error message with the entity name, rule, and error message', function () {
            //prepare test data
            const rule = 'TestRule';
            const errorMessage = 'Test error message';
            const expected = `${errorMessage}\n\tat entity: ${entityName}\n\tat mapping rule: ${rule}\n`;

            // prepare Stub functions
            const consoleLogSpy = sandbox.spy(console, 'log');

            //run tested function
            errorLogger.logParsingErrToConsole(rule, errorMessage, entityName);

            // check results
            assert(consoleLogSpy.calledWith(expected));
        });
    });

    describe('logEntityNameErrToConsole', function () {
        it('should log an error message with the entity name and current element', function () {
            //prepare test data
            const currentElement = 'TestElement';
            const expected = `SyntaxError: Entity name in mapping rules does not match any entities.\n\tat entity: ${entityName}\n\tname not found: ${currentElement}\n`;

            // prepare Stub functions
            const consoleLogSpy = sandbox.spy(console, 'log');

            //run tested function
            errorLogger.logEntityNameErrToConsole(currentElement, entityName);

            // check results
            assert(consoleLogSpy.calledWith(expected));
        });
    });

    describe('logMeaErrToConsole', function () {
        it('should log an error message with the entity name and current element', function () {
            //prepare test data
            const currentElement = 'TestElement';
            const expected = `SyntaxError: Measurement name (MEA_*) in mapping rules does not match any entities.\n\tat entity: ${entityName}\n\tmeasurement name not found: ${currentElement}\n`;

            // prepare Stub functions
            const consoleLogSpy = sandbox.spy(console, 'log');

            //run tested function
            errorLogger.logMeaErrToConsole(currentElement, entityName);

            // check results
            assert(consoleLogSpy.calledWith(expected));
        });
    });

    describe('logErrorAmtToConsole', function () {
        it('should log error message in singular', function () {
            //prepare test data
            const fname = 'unit-test.txt';
            const errorAmt = { value: 1 };

            // prepare Stub functions
            const logSpy = sandbox.spy(console, 'log');

            //run tested function
            errorLogger.logErrorAmtToConsole(fname, errorAmt);

            // check results
            assert(logSpy.calledOnce, 'console.log should be called once');
            assert(logSpy.calledWith(`\n1 error detected in file "${fname}".\t`), 'console.log should be called with the correct message');
        });

        it('should log error message in plural', function () {
            //prepare test data
            const fname = 'unit-test.txt';
            const errorAmt = { value: 3 };

            // prepare Stub functions
            const logSpy = sandbox.spy(console, 'log');

            //run tested function
            errorLogger.logErrorAmtToConsole(fname, errorAmt);

            // check results
            assert(logSpy.calledOnce, 'console.log should be called once');
            assert(logSpy.calledWith(`\n3 errors detected in file "${fname}".\t`), 'console.log should be called with the correct message');
        });
    });
});
