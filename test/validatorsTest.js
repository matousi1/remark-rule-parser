const assert = require('assert');
const sinon = require('sinon');
const validators = require('../src/validators');
const parser = require('../src/generatedParser');
const errorLogger = require('../src/errorMsgs');

describe('validatorsTest', () =>{
    let sandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    describe('identifierValidator', function () {
        it('returns 1 when identifier does not match file name', function () {
            //prepare test data
            const taggingSet = { identifiers: ['incorrect-identifier'] };
            const fname = 'file.json';

            //run tested function
            const result = validators.identifierValidator(taggingSet, fname);

            // check results
            assert.strictEqual(result, 1);
        });

        it('returns 0 when identifier matches file name', function () {
            //prepare test data
            const taggingSet = { identifiers: ['file'] };
            const fname = 'file.json';

            //run tested function
            const result = validators.identifierValidator(taggingSet, fname);

            // check results
            assert.strictEqual(result, 0);
        });

        it('returns 1 when no identifier is present', function () {
            //prepare test data
            const taggingSet = { identifiers: [] };
            const fname = 'file.json';

            //run tested function
            const result = validators.identifierValidator(taggingSet, fname);

            // check results
            assert.strictEqual(result, 1);
        });
    });

    describe('ruleValidator', function () {
        it('calls parser.parse and returns its result when rule is not empty', function () {
            //prepare test data
            const rule = 'rule';
            const name = 'name';
            const errorAmt = { value: 0 };

            // prepare Stub functions
            const parseStub = sandbox.stub(parser, 'parse').returns(true);
            const logParsingErrToConsoleStub = sandbox.spy(errorLogger, 'logParsingErrToConsole');

            //run tested function
            const result = validators.ruleValidator(rule, name, errorAmt);

            // check results
            assert.strictEqual(result, true);
            assert.strictEqual(parseStub.callCount, 1);
            assert.strictEqual(logParsingErrToConsoleStub.callCount, 0);
        });

        it('calls errorLogger.logParsingErrToConsole and increments errorAmt.value when parser throws an error', function () {
            //prepare test data
            const rule = 'rule';
            const name = 'name';
            const errorAmt = { value: 0 };
            const error = new Error('parse error');

            // prepare Stub functions
            const parseStub = sandbox.stub(parser, 'parse').throws(error);
            const logParsingErrToConsoleStub = sandbox.spy(errorLogger, 'logParsingErrToConsole');

            //run tested function
            const result = validators.ruleValidator(rule, name, errorAmt);

            // check results
            assert.strictEqual(result, undefined);
            assert.strictEqual(parseStub.callCount, 1);
            assert.strictEqual(logParsingErrToConsoleStub.callCount, 1);
            assert.strictEqual(errorAmt.value, 1);
        });
    });
//
    describe('getEntityName', function () {
        it('returns the name property of the taggingSet when it does not end with "Data"', function () {
            //prepare test data
            const taggingSet = { name: 'name' };

            //run tested function
            const result = validators.getEntityName(taggingSet);

            // check results
            assert.strictEqual(result, 'name');
        });

        it('returns undefined when the name property of the taggingSet ends with "Data"', function () {
            //prepare test data
            const taggingSet = { name: 'nameData' };

            //run tested function
            const result = validators.getEntityName(taggingSet);

            // check results
            assert.strictEqual(result, undefined);
        });
    });

    describe('meaValidator', () => {
        it('should return 0 when the currentElement is in namesMEA', () => {

            // prepare Stub functions
            const logMeaErrToConsoleStub = sandbox.spy(errorLogger, 'logMeaErrToConsole');

            //run tested function
            const result = validators.meaValidator('MEA_ISO', 'test-file');

            // check results
            assert.strictEqual(logMeaErrToConsoleStub.callCount, 0);
            assert.strictEqual(result, 0);
        });

        it('should log an error and return 1 when the currentElement is not in namesMEA', () => {

            // prepare Stub functions
            const logMeaErrToConsoleStub = sandbox.spy(errorLogger, 'logMeaErrToConsole');

            //run tested function
            const result = validators.meaValidator('invalid', 'test-file');

            // check results
            assert.strictEqual(logMeaErrToConsoleStub.callCount, 1);
            assert.strictEqual(result, 1);
        });
    });

    describe('entityNameValidator', () => {
        it('should return 0 when the currentElement is in entityNames', () => {

            // prepare Stub functions
            const logEntityNameErrToConsoleStub = sandbox.spy(errorLogger, 'logEntityNameErrToConsole');

            //run tested function
            const result = validators.entityNameValidator('Entity1', ['Entity1', 'Entity2'], 'test');

            // check results
            assert.strictEqual(result, 0);
            assert.strictEqual(logEntityNameErrToConsoleStub.callCount, 0);
        });

        it('should log an error and return 1 when the currentElement is not in entityNames', () => {

            // prepare Stub functions
            const logEntityNameErrToConsoleStub = sandbox.spy(errorLogger, 'logEntityNameErrToConsole');

            //run tested function
            const result = validators.entityNameValidator('invalid', ['Entity1', 'Entity2'], 'test');

            // check results
            assert.strictEqual(result, 1);
            assert.strictEqual(logEntityNameErrToConsoleStub.callCount, 1);
        });
    });

    describe('validateEntityNameAndMea', () => {
        it('should call entityNameValidator when the currentElement does not start with MEA_', () => {

            // prepare Stub functions
            const entityNameValidatorStub = sandbox.stub(validators, 'entityNameValidator');
            const meaValidatorStub = sandbox.stub(validators, 'meaValidator');

            //run tested function
            validators.validateEntityNameAndMea('Entity1', ['Entity1', 'Entity2'], 'test');

            // check results
            assert.strictEqual(entityNameValidatorStub.callCount,1);
            assert.strictEqual(meaValidatorStub.callCount, 0);
        });

        it('should call meaValidator when the currentElement starts with MEA_', () => {

            // prepare Stub functions
            const meaValidatorStub = sandbox.stub(validators, 'meaValidator');
            const entityNameValidatorStub = sandbox.stub(validators, 'entityNameValidator');

            //run tested function
            validators.validateEntityNameAndMea('MEA_ISO', ['Entity1', 'Entity2'], 'test');

            // check results
            assert.strictEqual(meaValidatorStub.callCount, 1);
            assert.strictEqual(entityNameValidatorStub.callCount,0);
        });
    });

      describe('parseEntityNameAndMea', function() { //incorrect
        it('inner Array discovered, runs recursion', function() {
            //prepare test data
            const currentEntities = {
                andOr: ['Entity1', { andOr: ['Entity2', 'MEA_ISO'] }]
            };
            const allEntityNames = ['Entity1', 'Entity2', 'MEA_ISO'];
            const name = 'Data';

            // prepare Stub functions
            const validateEntityNameAndMeaStub = sandbox.stub(validators, 'validateEntityNameAndMea');

            //run tested function
            validators.parseEntityNameAndMea(currentEntities, allEntityNames, name);

            // check results
            assert.strictEqual(validateEntityNameAndMeaStub.callCount, 3);
        });

        it('should return non-zero for invalid input', function() {
            //prepare test data
            const currentEntities = ['MEA_123'];
            const allEntityNames = ['Entity1', 'Entity2'];
            const name = 'Data';

            // prepare Stub functions
            const validateEntityNameAndMeaStub = sandbox.stub(validators, 'validateEntityNameAndMea');

            //run tested function
            const result = validators.parseEntityNameAndMea(currentEntities, allEntityNames, name);

            // check results
            assert.strictEqual(validateEntityNameAndMeaStub.callCount, 1);
            assert.notStrictEqual(result, 0);
        });
    });

    describe('entityNameAndMeaParser', function() {
        it('should return 0 if the name does not end with "Data"', function() {
            //prepare test data
            const parsedRule = {};
            const allEntityNames = ['Entity1', 'Entity2'];
            const name = 'invalidName';
            const errorAmt = {value: 0};

            // prepare Stub functions
            const parseEntityNameAndMeaStub = sandbox.stub(validators, 'parseEntityNameAndMea');

            //run tested function
            const result = validators.entityNameAndMeaParser(parsedRule, allEntityNames, name, errorAmt);

            // check results
            assert.equal(result, 0);
            assert.strictEqual(parseEntityNameAndMeaStub.callCount, 0);
        });

        it('should work', function() {
            //prepare test data
            const parsedRule = {
                impliedRule: {
                    entities: {
                        andOr: ['Entity1',['Entity2','MEA_ISO']]
                    }
                }
            };
            const allEntityNames = ['Entity1', 'Entity2'];
            const name = 'endingWith Data';
            const errorAmt = {value: 0};

            // prepare Stub functions
            const parseEntityNameAndMeaStub = sandbox.stub(validators, 'parseEntityNameAndMea');

            //run tested function
            const result = validators.entityNameAndMeaParser(parsedRule, allEntityNames, name, errorAmt);

            // check results
            assert.strictEqual(parseEntityNameAndMeaStub.callCount, 1);
        });
    });
});
