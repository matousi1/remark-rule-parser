const assert = require('assert');
const sinon = require('sinon');
const fs = require('fs');
const validators = require('../src/validators');
const errorLogger = require('../src/errorMsgs');
const filePreparator = require('../src/filePreparation');

describe('filePreparationTest', () =>{
    describe('prepareFile', () => {
        let sandbox;

        beforeEach(() => {
            sandbox = sinon.createSandbox();
        });

        afterEach(() => {
            sandbox.restore();
        });

        it('prepare the file without any errors', () => {

            //prepare test data
            const dir = "./test/testJSONs/";
            const fname = "unit-test.json";
            const errorAmt = { value: 0 };

            // prepare Stub functions
            const compileDataEntitiesStub = sandbox.stub(filePreparator,'compileDataEntities');
            const rawData = fs.readFileSync(dir + fname);
            const getEntityNameStub = sandbox.stub(validators, 'getEntityName');
            const logErrorAmtToConsoleStub = sandbox.stub(errorLogger, 'logErrorAmtToConsole');
            sandbox.stub(fs, 'readFileSync').returns(rawData);

            //run tested function
            filePreparator.prepareFile(dir, fname, errorAmt);

            // check results
            assert.strictEqual(getEntityNameStub.callCount, 3);
            assert(logErrorAmtToConsoleStub.calledWith(fname, errorAmt));
            assert.strictEqual(compileDataEntitiesStub.callCount, 3);
        });

        it('throw an error because the file was not found', () => {

            //prepare test data
            const dir = "./test/testJSONs/";
            const fname = 'nonexistent.json';
            const errorAmt = { value: 0 };

            // prepare Stub functions
            const consoleErrorStub = sandbox.stub(console,'error');
            const testError = new Error('file Not found');
            sandbox.stub(fs, 'readFileSync').throws(testError);

            try{
                //run tested function
                filePreparator.prepareFile(dir, fname, errorAmt);
                // check results
                assert.fail('Expected error to be thrown');
            } catch {
                // check results
                assert.ok(true);
            }
            // check results
            assert(consoleErrorStub.calledWith(testError));
        });
    });

    describe('compileDataEntities', () => {
        let sandbox;

        beforeEach(() => {
            sandbox = sinon.createSandbox();
        });

        afterEach(() => {
            sandbox.restore();
        });

        it('rules raw are not null', () => {
            //prepare test data
            const fname = 'nonexistent.json';
            const errorAmt = { value: 0 };
            const allEntityNames = ('Osoba', 'Vzdělání', 'Vzdělání Data');
            const taggingSet = {rules:"\"ds_kod\" = 430 -> \"Osoba\";\n\"ds_kod\" = 20 -> \"Vzdělání\" OR" +
                                      " \"MEA_Průměr\";\n\n(\"ds_kod\" = 430) -> (\"ds_cis\" -> \"Vzdělán" +
                                      "í\");\n(\"ds_kod\" = 20) -> (\"ds_cis\" -> \"Osoba\" AND \"MEA_ISO\");"};

            // prepare Stub functions
            const ruleValidatorStub = sandbox.stub(validators, 'ruleValidator');
            ruleValidatorStub.callsFake(() => {
                const index = ruleValidatorStub.callCount - 1;
                const results = ['rule1', 'rule2', 'rule3','rule4','rule5'];
                return results[index];
            });
            const entityNameAndMeaParserStub = sandbox.stub(validators, 'entityNameAndMeaParser');
            const identifierValidatorStub = sandbox.stub(validators, 'identifierValidator').returns(0);

            //run tested function
            filePreparator.compileDataEntities(taggingSet, fname, errorAmt, allEntityNames);

            // check results
            assert.strictEqual(ruleValidatorStub.callCount,5);
            assert.strictEqual(entityNameAndMeaParserStub.callCount,5);
            assert.strictEqual(identifierValidatorStub.callCount,1);
        });

        it('rules raw are null', () => {
            //prepare test data
            const fname = 'nonexistent.json';
            const errorAmt = { value: 0 };
            const allEntityNames = ('Osoba', 'Vzdělání', 'Vzdělání Data');
            const taggingSet = {rules:null};

            // prepare Stub functions
            const ruleValidatorStub = sandbox.stub(validators, 'ruleValidator');
            const entityNameAndMeaParserStub = sandbox.stub(validators, 'entityNameAndMeaParser');
            const identifierValidatorStub = sandbox.stub(validators, 'identifierValidator').returns(0);

            //run tested function
            filePreparator.compileDataEntities(taggingSet, fname, errorAmt, allEntityNames);

            // check results
            assert.strictEqual(ruleValidatorStub.callCount, 0);
            assert.strictEqual(entityNameAndMeaParserStub.callCount, 0);
            assert.strictEqual(identifierValidatorStub.callCount, 0);
        });
    });
});